""" Used functions for bjt.ipynp

They are given here to be able to test them separatly and also to re easily reusable

"""
# imports
import numpy as np

## constants
EPS_SI = 11.7 * 8.85e-12 # relative permittivity of silicon in As/Vm
Q = 1.602e-19 # elementary charge in C
K_B = 1.38e-23 # boltzmann constant in J/K
N_I0 = 1.07e10*1e6 # intrinsic carrier concentration in 1/m^3
V_NS = 1.07e7*1e-2 # saturation velocity of electrons in m/s

## functions
def calc_vt(T):
    """ Calculates the thermal voltage

    Parameter
    ----------
    T : float
        Temperature in Kelvin

    Returns
    --------
    V_T : float
        Thermal voltage in V
    """
    return K_B * T / Q

def get_doping_profile(w_Em, w_Bm, w_Cm, N_E, N_B, N_C):
    """ Returns the x axis and the doping profile along the axis

    Parameter
    ------------
    w_Em, w_Bm, w_Cm : float
        Widths of the metallurgic emitter/base/collector region in m
    N_E, N_B, N_C : float
        Emitter/base/collector doping in m^-3

    Returns
    --------
    x : np.array
    profile : np.array
    """
    # wBm = wbm_slider.value*1e-6 # metallic base witdh in m
    x = np.linspace(0, w_Em+w_Bm+w_Cm, 1001)
    profile = np.zeros(1001)
    profile[x<w_Em                                           ] = N_E
    profile[np.logical_and(x>=w_Em       ,x<(w_Em+w_Bm)     )] = N_B
    profile[np.logical_and(x>=(w_Em+w_Bm),x<(w_Em+w_Bm+w_Cm))] = N_C

    return x, profile

def get_basics_mobGT(carrier, N_D, N_A, T):
    """ Calculates the mobility

    Parameter
    --------------
    carrier : {"n", "p"}
        Carrier type
    N_A, N_D : float
        Acceptor and donator doping in 1/m^3
    T : float
        Temperature in Kelvin

    Returns
    --------------
    mob : float
        Mobility in m^2/Vs
    """
    # returns mob in µm^2/Vs
    G = N_D + N_A

    if carrier == "n":
        N = N_A
        c = N_D

        mu_0 = 80.0*1e-4 # electron mobility for impurity scattering in m^2/Vs
        r_mu = 2.8 # minority to majority mobility ratio at high doping
        mu_max  = 1360.0*1e-4 # mobility for lattice scattering in m^2/Vs
        G_mu = 1e17*1e6 # threshold doping concentration in 1/m^3
        beta_G = 0.77 # doping dependence
        mu_m1 = 0.0 # mobility at very high doping in m^2/Vs
        G_mu1 = 3.43e20*1e6 # threshold doping concentration for very-high-doping effects in 1/m^3
        beta_m1 = 2.0 # field dependence
        zeta_T = -2.5 # base temperature dependence
        beta_T = 0.35 # doping dependency of temperature dependence
    elif carrier == "p":
        N = N_D
        c = N_A

        mu_0 = 30.0*1e-4 # hole mobility for impurity scattering
        r_mu = 4.3 # minority to majority mobility ratio at high doping
        mu_max = 470.0*1e-4 # mobility for lattice scattering in m^2/Vs
        G_mu = 3.2e17*1e6 # threshold doping concentration in 1/m^3
        beta_G = 0.63 # doping dependence
        mu_m1 = 0.0 # mobility at very high doping in m^2/Vs
        G_mu1 = 6.1e20*1e6 # threshold doping concentration for very-high-doping effects in 1/m^3
        beta_m1 = 2.0 # field dependence
        zeta_T = -2.5 # base temperature dependence
        beta_T = 0.35 # doping dependency of temperature dependence
    else:
        raise IOError("Only Electron and holes are implemented as carriers.")

    mu_I = mu_0 * (1.0 + (r_mu - 1.0) * N/ (N + c))
    mu_G = mu_I + (mu_max - mu_I) / (1.0 + np.float_power(G / G_mu, beta_G)) - mu_m1 / (1.0 + np.float_power(G_mu1 / G, beta_m1))

    # temperature dependence
    T_0 = 300.0 # reference temperature
    zeta = zeta_T / (1.0 + np.float_power(G / G_mu, beta_T))
    mu_T = mu_G * np.float_power(T / T_0, zeta)
    return mu_T

def get_basics_niTSi(T):
    """ Intrinsic carrier density of silicon at given temperature

    Parameter
    -------------
    T : float
        Temperature in K

    Returns
    ------------
    ni : float
        Intrinsic carrier density in 1/m^3
    """
    Eg0 = 1.2*1.60218e-19
    T0 = 300
    ni = N_I0 * np.float_power(T / T0, 1.5) * np.exp(Eg0/2.0/K_B/T0 * (1.0 - T0/T))
    return ni

def calc_built_in_voltage(N_A, N_D, T):
    """ Calculates the build-in voltage

    Parameter
    --------------
    N_A, N_D : float
        Acceptor and donator doping in 1/m^3
    T : float
        Temperature in K

    Returns
    --------------
    U_D : float
        build-in voltage in V
    """
    U_T = calc_vt(T)
    ni = get_basics_niTSi(T)

    U_D = U_T * np.log(N_D * N_A / ni / ni)
    return U_D


@np.vectorize
def get_junction_xn_xp(N_A, N_D, U_j, T):
    """ Calculates the locations of the junction borders

    Parameter
    --------------
    N_A, N_D : float
        Acceptor and donator doping in 1/m^3
    U_j : float
        Applied junction voltage in V
    T : float
        Temperature in K

    Returns
    --------------
    xn, xp : float
        Distances to the two different borders in m
    """
    U_D = calc_built_in_voltage(N_A, N_D, T)

    arg = 2.0 * EPS_SI / Q * N_A * (U_D - U_j) / N_D / (N_A + N_D)
    if arg >= 0.0:
        xn = np.sqrt(arg)
    else:
        xn = 0.0

    arg = 2.0 * EPS_SI / Q * N_D * (U_D - U_j) / N_A / (N_A + N_D)
    if arg >= 0.0:
        xp = np.sqrt(arg)
    else:
        xp = 0.0

    return xn, xp

def calc_currents(N_E, N_B, w_Em, w_Bm, vbe, vbc, T):
    """ Calculates the collector and base current densities

    Parameter
    ----------
    N_E, N_B : float
        Acceptor and donator doping in 1/m^3
    w_Em, w_Bm : float
        Widths of the metallurgic emitter/base region in m
    vbe, vbc : float or np.array
        Applied voltages in V
    T : float
        Temperature in K

    Returns
    --------
    jc, jb : float or np.array
        Collector and base current density in A/m^2
    """
    ni = get_basics_niTSi(T)
    U_T = calc_vt(T)

    mu_nB = get_basics_mobGT('n', 0, N_B, T)
    mu_pE = get_basics_mobGT('p', N_E, 0, T)

    #js  = q * U_T * mu_nB * ni * ni / NB / wB
    #JC  = js * (np.exp(vbe / U_T) - np.exp(vbc / U_T))
    # collector current from electron density
    # ne = NB/2 * (np.sqrt(1+4*ni*ni/NB/NB * np.exp(vbe/U_T)) - 1)
    ne_low = ni * ni / N_B * (np.exp(vbe / U_T) - np.exp(vbc / U_T))
    ne_high = ni * (np.exp(vbe / 2 / U_T) - np.exp(vbc / 2 / U_T))
    ne = np.where(
        ne_low < ne_high,
        ne_low,
        ne_high
    )
    JC = Q*mu_nB*U_T * ne / w_Bm

    xn, xp = get_junction_xn_xp(N_B, N_E, vbe, T)

    wE  = w_Em - xn
    jBs = Q * U_T * mu_pE * ni * ni / N_E / wE
    JB  = jBs * (np.exp(vbe / U_T) - 1.0)
    return JC, JB

def calc_capacity(N_A, N_D, U_j, T):
    """ Calculates the capacity per area of the given junction

    Parameter
    --------------
    N_A, N_D : float
        Acceptor and donator doping in 1/m^3
    U_j : float
        Applied junction voltage in V
    T : float
        Temperature in K

    Returns
    ---------
    c_j : float or np.array
        capacity per area of the given junction
    """
    U_D = calc_built_in_voltage(N_A, N_D, T)

    arg = Q*EPS_SI * N_A*N_D / (2.0*U_D * (N_A+N_D))
    if arg > 0:
        cj0 = np.sqrt(arg)
    else:
        cj0 = 0

    # temperature scaling
    U_D0 = calc_built_in_voltage(N_A, N_D, 300.00)
    cj0_t = cj0 * np.sqrt(U_D0/U_D)

    # cap of the applied voltage to not be larger than the build-in voltage
    U_j_cap = np.where(U_j < U_D, U_j, U_D-2e-3)

    return cj0_t / np.float_power(1-U_j_cap/U_D, 0.5)

def calc_tau_f(N_E, N_B, N_C, w_Bm, vbe, vbc, T):
    """ calculates the forward transit time

    Parameter
    ----------
    N_E, N_B, N_C : float
        Emitter/base/collector doping in 1/m^3
    w_Bm : float
        Widths of the metallurgic base region in m
    vbe, vbc : float or np.array
        Applied voltages in V
    T : float
        Temperature in K

    Returns
    -------
    tau_f : float or np.array
        Forward transit time
    """
    U_T = calc_vt(T)
    mu_nB = get_basics_mobGT('n', 0, N_B, T)

    # BE space charge region
    xn_be, xp_be = get_junction_xn_xp(N_B, N_E, vbe, T)
    # BC space charge region
    xn_bc, xp_bc = get_junction_xn_xp(N_B, N_C, vbc, T)

    w_B = w_Bm - xn_be - xn_bc
    tau_B = w_B*w_B/(2*mu_nB*U_T)

    w_BC = xn_bc + xp_bc
    tau_BC = w_BC/2/V_NS

    return tau_B + tau_BC

def calc_ft(N_E, N_B, N_C, w_Em, w_Bm, vbe, vbc, T):
    """ calculates the forward transit frequency

    Parameter
    ----------
    N_E, N_B, N_C : float
        Emitter/base/collector doping in 1/m^3
    w_Em, w_Bm : float
        Widths of the metallurgic base region in m
    vbe, vbc : float or np.array
        Applied voltages in V
    T : float
        Temperature in K

    Returns
    -------
    f_T : float or np.array
        Forward transit frequency
    """
    tau_f = calc_tau_f(N_E, N_B, N_C, w_Bm, vbe, vbc, T)

    # numerical derivative for gm using the symmetric difference quotient
    h = 1e-12
    jc_0, jb = calc_currents(N_E, N_B, w_Em, w_Bm, vbe-h, vbc, T)
    jc_1, jb = calc_currents(N_E, N_B, w_Em, w_Bm, vbe+h, vbc, T)
    gm = (jc_1-jc_0)/(2*h)

    c_je = calc_capacity(N_B, N_E, vbe, T)
    c_jc = calc_capacity(N_B, N_C, vbc, T)

    return 1/(2*np.pi*tau_f + (c_je + c_jc)/gm)
